from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers

# urlpatterns = patterns('',
#     # Examples:
#     # url(r'^$', 'mysite.views.home', name='home'),
#     # url(r'^blog/', include('blog.urls')),
#
#     url(r'^polls/', include('polls.urls')),
#     url(r'^admin/', include(admin.site.urls)),
# )

router = routers.DefaultRouter()

urlpatterns = [
    # Added: polls urls with a namespace for a better and easier accessibility
    url(r'^polls/', include('polls.urls', namespace="polls")),
    url(r'^admin/', include(admin.site.urls)),
    # Wiring up the API with an automatic URL routing.
    url(r'^', include(router.urls)),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
