from rest_framework import serializers
from .models import Question, Choice


class QuestionSerializer(serializers.ModelSerializer):
    choice_set = serializers.StringRelatedField(many=True)

    class Meta:
        model = Question
        fields = ('id', 'question_text', 'pub_date', 'choice_set')

    def create(self, validated_data):
        """
        Create and return a new Question instance, given the validated data.
        """
        return Question.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing Question instance, given the validated data.
        """
        instance.id = validated_data.get('id', instance.id)
        instance.question_text = validated_data.get('question_text', instance.question_text)
        instance.pub_date = validated_data.get('pub_date', instance.pub_date)
        instance.save()
        return instance


class ChoiceSerializer(serializers.ModelSerializer):
    question = serializers.PrimaryKeyRelatedField(queryset=Question.objects.all())

    class Meta:
        model = Choice
        fields = ('id', 'question', 'choice_text', 'votes')

    def create(self, validated_data):
        return Choice.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.id = validated_data.get('id', instance.id)
        instance.question = validated_data.get('question', instance.question)
        instance.choice_text = validated_data.get('choice_text', instance.choice_text)
        instance.voites = validated_data.get('votes', instance.votes)
        instance.save()
        return instance
