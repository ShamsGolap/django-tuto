from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

# The url used to access to different views
# Use a regular expression to check the url and redirect
# to the correct view, with namespace (param 'name').

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    url(r'^apiq/$', views.QuestionsList.as_view(), name='questionslistapi'),
    url(r'^apic/$', views.ChoiceList.as_view(), name='choiceslistapi'),
    url(r'^apiq/(?P<pk>[0-9]+)/$', views.QuestionsDetail.as_view(), name='questionsdetailapi'),
    url(r'^apic/(?P<pk>[0-9]+)/$', views.ChoiceDetail.as_view(), name='choicesdetailapi'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
