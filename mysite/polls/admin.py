from django.contrib import admin

from .models import Choice, Question


# Changing the way the choices are visible from the admin page in an
# "inline" view, more compact, table-based format.
class ChoiceInline(admin.TabularInline):
    # use the model "Choice"
    model = Choice
    # the amount of slots visible
    extra = 3


# Change the admin view for displaying questions.
class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']

# Say to the admin page that there are Question objects on the admin interface.
# QuestionAdmin is for override the default form.
admin.site.register(Question, QuestionAdmin)
