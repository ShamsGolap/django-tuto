import datetime

from django.db import models
from django.utils import timezone


# Question model:
# question_text: concretely, the question.
# pub_date: date of publication on the site.
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    # "To stringify" the question
    def __str__(self):
        return self.question_text

    # Check if the question was published within one day.
    # Return false if a question is programmed to be shown in the future.
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'


# Choice model:
# question: the question that the choice is connected to
# choice_text: the choice, litteraly
# votes: number of votes of a choice
class Choice(models.Model):
    question = models.ForeignKey(Question, related_name='choice_set')
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    # "To stringify" the choice
    def __str__(self):
        return self.choice_text
